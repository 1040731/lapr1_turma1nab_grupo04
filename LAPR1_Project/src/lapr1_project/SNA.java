/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1_project;
import java.io.IOException;
import org.la4j.*;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.decomposition.EigenDecompositor;

/**
 * @authors
 *
 * LAPR1 2018/2019 Grupo 04
 *
 * 1970570 Claudia Monteiro 1040321 Pedro Barbosa 1040731 Pedro Sequeira 1080510
 * Miguel Silva
 *
 * Classe principal para o cálculo dos métodos pedidos no enunciado.
 */
public class SNA {

    public static int matriz[][] = new int[FileManager.sizeMatriz][FileManager.sizeMatriz];

    /**
     * Lista uma matriz
     */
    public static void listar() {
        Utils.listarMatriz(matriz, FileManager.sizeMatriz);
    }

    /**
     * Dado o índice i soma os valores da linha i, utilizado para calcular 
     * o grau do nó em redes não orientadas ou o grau de entrada do nó em redes
     * orientadas
     * 
     * @return int grau do nó / grau de entrada do nó
     */
    public static int calculateNodeDegree(int nodeIndex, int[][] mat) {

        int result = 0;

        for (int i = 0; i < mat.length; i++) {
            result = result + mat[nodeIndex][i];
        }

        return result;
    }

    /**
     * Vertices com maior peso. Através da LA4j obtemos uma array de valores
     * próprios onde se verifica o index do(s) valores maiores que vai
     * correponder ao index da linha (vertice) da matriz
     *
     * @param matriz
     */
    public static double[] calculateEigenVecCent(int[][] matriz) {
       return calculateEigenVecCent(Utils.copiarMatrizToDouble(matriz));
    }
    
    public static double[] calculateEigenVecCent(double[][] matriz) {
        // Criar objeto do tipo Matriz após duplicar a matriz para o tipo de dados Double              
        Matrix a = new Basic2DMatrix(matriz);

        //Obtem valores e vetores próprios fazendo "Eigen Decomposition"
        EigenDecompositor eigenD = new EigenDecompositor(a);

        Matrix[] mattD = eigenD.decompose();

        // converte objeto Matrix (duas matrizes)  para array Java;  0- Matriz de Vectores Proprios;   1- Matriz de Valores Proprios   
        double matVecProprios[][] = mattD[0].toDenseMatrix().toArray();

        double matValoresProprios[][] = mattD[1].toDenseMatrix().toArray();


        int indexValMax = Utils.calcularIndexMaiorDiagonalPrincipal(matValoresProprios);

        //Validar se encontrou valor max
        if (indexValMax > -1) {
            double[] vecProprio = Utils.getColunaMatriz(matVecProprios, indexValMax, matVecProprios.length);
            //Validar se o vector está normalizado
            if (Utils.isNormalizado(vecProprio)) {
                return vecProprio;
            } else {
                return Utils.nomalizar(vecProprio);
            }
        }
        return new double[0];
    }


    /**
     * Somatorio das arestas (que correspondem aos valores > 0 da matriz) a
     * dividir pelo numero de linhas (vertices);
     * 
     * @return double grau médio da rede
     */
    public static double calculateAverageDegree(int[][] matriz) {
        int sum = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (matriz[i][j] != 0) {
                    sum += matriz[i][j];
                }
            }
        }
        double res = (double) sum / matriz.length;

        return res;
    }

    /**
     * A razao entre o numero de arestas existentes pelo maior numero de arestas
     * possiveis para o grafo
     * 
     * @ return double densidade da rede
     */
    public static double calculateDensity(int matriz[][]) {

        int n = matriz.length;

        int max = (n * (n - 1)) / 2;

        int counter = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (matriz[i][j] != 0) {
                    counter++;
                }
            }
        }
         
        //Counter dividido por 2 para representar apenas uma ligação entre 2 nós
        double res = (double) (counter / 2) / max;

        return res;
    }

    /**
     * Dado um valor para o tamanho do caminho, a matriz de adjacências e o par
     * de nós, calcula o número de caminhos desse tamanho para o par de nós m e
     * 
     * @return int valor do número de caminhos para o par de nós
     */
    public static int calculatePowersAMforNode(int k, int m, int n, int[][] mat) {

        int matTemp[][] = new int[mat.length][mat.length];

        matTemp = Utils.copyMatrix(mat);

        int matResult[][] = new int[mat.length][mat.length];
        if (k == 1) {
            matResult = mat;
        } else {
            for (int a = 0; a < k - 1; a++) {
                matResult = Utils.multiplyMatrix(mat, matTemp);
                matTemp = Utils.copyMatrix(matResult);
            }
        }

        return matResult[m][n];
    }

    /**
     * Dado um valor para o tamanho do caminho e a matriz de adjacências,
     * calcula o número de caminhos desse tamanho para cada par de nós
     * 
     * @ return int[][] Matriz com o resultado da  Potência da matriz de Adjacências de ordem k
     */
    public static int[][] calculatePowersAM(int k, int[][] mat) {

        int matTemp[][] = new int[mat.length][mat.length];

        matTemp = Utils.copyMatrix(mat);

        int matResult[][] = new int[mat.length][mat.length];

        if (k == 1) {
            matResult = mat;
        } else {

            for (int a = 0; a < k - 1; a++) {

                matResult = Utils.multiplyMatrix(mat, matTemp);
                matTemp = Utils.copyMatrix(matResult);
            }
        }
        return matResult;
    }

    /**
     * Calcula todos os parametros para serem guardados em ficheiro para redes não orientadas
     *
     * @param matriz matiz da rede para o calculo
     * @param elem numero de elementos da matriz
     * @throws IOException
     */
    public static void calculateAll(int matriz[][], int elem, int potencia) throws IOException {
        int nodeDegrees[] = new int[elem];
        double eigenVec[] = new double[elem];
        double averageDegree;
        double density;
       
        for (int i = 0; i < elem; i++) {
            nodeDegrees[i] = calculateNodeDegree(i, matriz);
        }
        eigenVec = calculateEigenVecCent(matriz);
        averageDegree = calculateAverageDegree(matriz);
        density = calculateDensity(matriz);
        

        FileManager.guardarEmFicheiro(nodeDegrees, eigenVec, averageDegree, density, matriz, potencia);
    }

    /**
     * Dado o índice i soma os valores da coluna i, utilizado para calcular 
     * o grau de saída do nó em redes orientadas
     * 
     * @return int grau de saída do nó
     */
    public static int calculateNodeOutDegree(int nodeIndex, int[][] mat) {

        int result = 0;

        for (int i = 0; i < mat.length; i++) {
            result = result + mat[i][nodeIndex];
        }

        return result;
    }
    
    
    /**
     * Dado o número de iterações k, o damping factor e a matriz de adjacência de 
     * uma rede orientada, devolve a matriz com os valores de Page Rank para cada iteração
     * através do método iterativo
     *
     * @return double[][] Page Rank - metodo iterativo
     */
    public static double[][] calculatePageRankIteration(int[][] matAdj, int k, double dampingFactor) {
        double[][] matrix = calculateMatrixForReducibleWebGraph(matAdj, dampingFactor);

        int dimention = matAdj.length;

        double[][] matPageRank = new double[k][dimention];

        matPageRank[0] = Utils.fillArray((double) 1 / dimention, dimention);

        for (int i = 1; i < k; i++) {
            matPageRank[i] = Utils.multiplyMatrixVector(matrix, matPageRank[i - 1]);
            if ((i + 1) == k) {
                if (!Utils.isNormalizado(matPageRank[i])) {
                    matPageRank[i] = Utils.nomalizar(matPageRank[i]);
                }

            }
        }

        return matPageRank;
    }

    /**
     * Dado o damping factor e a matriz de adjacência de uma rede orientada, 
     * devolve a matriz com os valores de Page Rank através do método da 
     * centralidade do vetor próprio
     * 
     * @return double[][] valores de Page Rank - metodo centralidade do vetor proprio
     */
    public static double[] calculatePageRankEigenVec(int[][] matAdj, double dampingFactor) {
        double[][] matrix = calculateMatrixForReducibleWebGraph(matAdj,dampingFactor);  
        return calculateEigenVecCent(matrix);
    }

    /**
     * Dada a matriz de adjacência de uma rede orientada, calcula a Matriz cujos
     * coeficiente são calculados para reduzir o impacto do nós sem saída e 
     * nós sem percurso entre eles
     * 
     * @return double[][] Matriz com redução de impacto de Dangling Nodes e nós sem percurso 
     * entre eles
     */    
    public static double[][] calculateNotDanglingMatrix(int[][] matAdj) {
        int matDimention = matAdj.length;
        double[][] matrix = new double[matDimention][matDimention];
        for (int col = 0; col < matDimention; col++) {
            int nodeOutDegree = calculateNodeOutDegree(col, matAdj);

            for (int row = 0; row < matDimention; row++) {
                if (nodeOutDegree == 0) {
                    matrix[row][col] = (double) 1 / matDimention;
                } else if (matAdj[row][col] == 1) {
                    matrix[row][col] = (double) 1 / nodeOutDegree;
                }
            }

        }
        return matrix;
    }
    /**
     * Dada a matriz de adjacência de uma rede orientada, calcula a Matriz M, cujos
     * coeficiente são calculados para reduzir o impacto do nós sem saída e 
     * nós sem percurso entre eles com aplicação de do fator de amortização  damping factor  
     * 
     * @return double[][] Matriz M 
     */
    public static double[][] calculateMatrixForReducibleWebGraph(int[][] matAjd, double dampingFactor){
        int dimention = matAjd.length;
 
        double[][] notDanglingMatrix = calculateNotDanglingMatrix(matAjd);

        double[][] mat1 = Utils.matrixTimesScalar(notDanglingMatrix, dampingFactor);

        double[][] mat2 = Utils.fillMatrixNxN((double) (1 - dampingFactor) / dimention, dimention);

        return Utils.matrixSum(mat1, mat2);
    }    
    /**
     * Calcula todos os parametros para serem guardados em ficheiro para redes orientadas
     *
     * @param matriz matiz da rede para o calculo
     * @param elem numero de elementos da matriz
     * @param iteracoes numero de iteracoes do metodo iterativo de calculo do Page Rank
     * @param damp damping factor
     * @throws IOException
     */
    public static void calculateAllOriented(int matriz[][], int elem, int iteracoes, double damp) throws IOException {
        int inDegrees[] = new int[elem];
        int outDegrees[] = new int[elem];
        for (int i = 0; i < elem; i++) {
            inDegrees[i] = calculateNodeDegree(i, matriz);
        }
        for (int j = 0; j < elem; j++) {
            outDegrees[j] = calculateNodeOutDegree(j, matriz);
        }
        double pageRankeigenVec[] = calculatePageRankEigenVec(matriz, damp);
        double pageRank[][] = calculatePageRankIteration(matriz, iteracoes, damp);

        FileManager.guardarEmFicheiroOiented(inDegrees, outDegrees, pageRankeigenVec, pageRank, matriz, iteracoes, damp);
    }

    /**
     * Calculo da distancia euclidiana 
     * 
     */
    public static void calculateEuclidianeDistance(int[][] matAdj, double dampingFactor, int k) {
        double[] eigenVec = calculatePageRankEigenVec(matAdj, dampingFactor);
        double[][] iterPageRank = calculatePageRankIteration(matAdj, k, dampingFactor);
        for (int i = 0; i < iterPageRank.length; i++) {
            double soma = 0;
            for (int j = 0; j < eigenVec.length; j++) {
                double valor = eigenVec[j] - iterPageRank[i][j];
                soma += Math.pow(valor, 2);
            }
            System.out.printf("%s%d%s%.3f\n", "Distancia euclidiana da Iteração ", i, " é ", Math.sqrt(soma));
        }
    }
}
