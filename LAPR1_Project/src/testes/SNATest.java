/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes;

import lapr1_project.FileManager;
import lapr1_project.SNA;

/**
 * @authors 
 * 
 * LAPR1 2018/2019 
 * Grupo 04 
 * 
 * 1970570 Claudia Monteiro 
 * 1040321 Pedro Barbosa 
 * 1040731 Pedro Sequeira 
 * 1080510 Miguel Silva
 *
 * Classe de testes da Classe SNA.
 */
public class SNATest {

    private static double arrDoubleTest[] = new double[]{1.0, 2.0, 1.0, 3.0};

    public static int testMatAdjOriented[][] = {
        {0, 0, 1, 0},
        {1, 0, 0, 0},
        {1, 0, 0, 0},
        {1, 1, 1, 0},};

    public static int matOriented[][] = {
        {0, 0, 1, 0, 0, 0, 0, 0},
        {1, 0, 0, 1, 0, 0, 0, 0},
        {1, 0, 0, 0, 0, 0, 0, 0},
        {1, 1, 1, 0, 0, 0, 0, 0},
        {0, 1, 0, 0, 0, 1, 0, 0},
        {0, 0, 0, 0, 0, 0, 1, 1},
        {0, 0, 0, 1, 1, 0, 0, 1},
        {0, 0, 0, 0, 0, 1, 0, 0},};

    public static int A[][] = {
        {0, 1, 1, 0, 0, 1, 0},
        {1, 0, 0, 0, 0, 0, 0},
        {1, 0, 0, 1, 1, 1, 0},
        {0, 0, 1, 0, 1, 0, 1},
        {0, 0, 1, 1, 0, 0, 0},
        {1, 0, 1, 0, 0, 0, 1},
        {0, 0, 0, 1, 0, 1, 0},};

    private static int matTest[][] = new int[][]{
        {0, 1, 0, 0},
        {1, 0, 1, 1},
        {0, 1, 0, 1},
        {0, 1, 1, 0},};

    private static boolean compararVecDouble(double arr1[], double arr2[]) {

        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }

    private static boolean compararMatDouble(double mat1[][], double mat2[][]) {
        boolean flag = true;
        for (int i = 0; i < mat1.length; i++) {
            for (int j = 0; j < mat1[i].length; j++) {
                if (mat1[i][j] != mat2[i][j]) {
                    flag = false;
                }
            }
        }
        return flag;
    }

    /**
     * Test of calculateNodeDegree method, of class SNA.
     */
    public static boolean testCalculateNodeDegree() {

        boolean bol;
        if (SNA.calculateNodeDegree(1, A) == 1) {

            bol = true;
        } else {

            bol = false;
        }
        return bol;
    }

    /**
     * Test of calculateEigenVecCent method, of class SNA.
     */
    public static boolean testCalculateEigenVecCent() {

        double[] expected = new double[]{0.2818452038510028, 0.6116284589096466, 0.522720723387737, 0.522720723387737};

        double[] vectorProprio = SNA.calculateEigenVecCent(matTest);

        return compararVecDouble(expected, vectorProprio);
    }

    /**
     * Test of calculateAverageDegree method, of class SNA.
     */
    public static boolean testCalculateAverageDegree() {

        boolean ret;
        double expected = 2.5714285714285716;

        double res = SNA.calculateAverageDegree(A);

        if (res == expected) {
            ret = true;
        } else {
            ret = false;
        }

        return ret;
    }

    /**
     * Test of calculateDensity method, of class SNA.
     */
    public static boolean testCalculateDensity() {

        boolean ret;
        double expected = 0.42857142857142855;

        double res = SNA.calculateDensity(A);
        if (res == expected) {
            ret = true;
        } else {
            ret = false;
        }

        return ret;
    }

    /**
     * Test of calculatePowersAM method, of class SNA.
     *
     */
    public static boolean testCalculatePowersAM() {

        boolean bol = true;
        int[][] matResult = {
            {2, 3, 6, 3, 2, 5, 2},
            {3, 0, 1, 1, 1, 1, 1},
            {6, 1, 4, 7, 5, 7, 2},
            {3, 1, 7, 2, 4, 2, 5},
            {2, 1, 5, 4, 2, 3, 2},
            {5, 1, 7, 2, 3, 2, 5},
            {2, 1, 2, 5, 2, 5, 0},};

        int[][] matTest = SNA.calculatePowersAM(3, A);

        for (int i = 0; i < matTest.length; i++) {
            for (int j = 0; j < matTest[0].length; j++) {

                if (matResult[i][j] != matTest[i][j]) {

                    bol = false;
                }
            }
        }

        return bol;

    }

    /**
     * Test of calculateNodeDegree method, of class SNA.
     */
    public static boolean testCalculateOutNodeDegree() {

        boolean bol;
        if (SNA.calculateNodeOutDegree(1, A) == 1) {

            bol = true;
        } else {

            bol = false;
        }
        return bol;
    }

    public static boolean testCalculateMatrixForReducibleWebGraph() {
        double[][] expectedRes = new double[][]{
            {0.037500000000000006, 0.3208333333333333, 0.037500000000000006, 0.037500000000000006},
            {0.8875, 0.037500000000000006, 0.4625, 0.4625},
            {0.037500000000000006, 0.3208333333333333, 0.037500000000000006, 0.4625},
            {0.037500000000000006, 0.3208333333333333, 0.4625, 0.037500000000000006},};

        double[][] res = SNA.calculateMatrixForReducibleWebGraph(matTest, 0.85);

        return compararMatDouble(expectedRes, res);
    }

    public static boolean TestCalculateNotDanglingMatrix() {
        double[][] expectedRes = new double[][]{
            {0.0, 0.3333333333333333, 0.0, 0.0},
            {1.0, 0.0, 0.5, 0.5},
            {0.0, 0.3333333333333333, 0.0, 0.5},
            {0.0, 0.3333333333333333, 0.5, 0.0},};

        double[][] res = SNA.calculateNotDanglingMatrix(matTest);

        return compararMatDouble(expectedRes, res);
    }

    public static boolean TestCalculatePageRankIteration() {
        double[][] expectedRes = new double[][]{
            {0.25, 0.25, 0.25, 0.25},
            {0.10833333333333331, 0.4625, 0.21458333333333332, 0.21458333333333332},
            {0.16854166666666667, 0.31197916666666664, 0.25973958333333336, 0.25973958333333336},
            {0.23428792047334387, 0.7472610234924919, 0.439721578438692, 0.4397215784386919},};

        double[][] res = SNA.calculatePageRankIteration(matTest, 4, 0.85);

        return compararMatDouble(expectedRes, res);
    }

    public static boolean TestCalculatePageRankEigenVec() {
        double[] expectedRes = new double[]{0.26943382048726366, 0.6987631493495212, 0.46858055736915494, 0.468580557369155};

        double[] res = SNA.calculatePageRankEigenVec(matTest, 0.85);

        return compararVecDouble(expectedRes, res);
    }

    public static void runAll() {
        String str = "";

        System.out.print("\nTeste CalculateNodeDegree() - ");
        str = testCalculateNodeDegree() ? "OK\n" : "NOT OK\n";
        System.out.print(str);

        System.out.print("\nTeste CalculateEigenVecCent() - ");
        str = testCalculateEigenVecCent() ? "OK\n" : "NOT OK\n";
        System.out.print(str);

        System.out.print("\nTeste CalculateAverageDegree() - ");
        str = testCalculateAverageDegree() ? "OK\n" : "NOT OK\n";
        System.out.print(str);

        System.out.print("\nTeste CalculateDensity() - ");
        str = testCalculateDensity() ? "OK\n" : "NOT OK\n";
        System.out.print(str);

        System.out.print("\nTeste CalculatePowersAM() - ");
        str = testCalculatePowersAM() ? "OK\n" : "NOT OK\n";
        System.out.print(str);

        System.out.print("\nTeste CalculateOutNodeDegree() - ");
        str = testCalculateOutNodeDegree() ? "OK\n" : "NOT OK\n";
        System.out.print(str);

        System.out.print("\nTeste CalculateMatrixForReducibleWebGraph() - ");
        str = testCalculateMatrixForReducibleWebGraph() ? "OK\n" : "NOT OK\n";
        System.out.print(str);

        System.out.print("\nTeste CalculateNotDanglingMatrix() - ");
        str = TestCalculateNotDanglingMatrix() ? "OK\n" : "NOT OK\n";
        System.out.print(str);

        System.out.print("\nTeste CalculatePageRankIteration() - ");
        str = TestCalculatePageRankIteration() ? "OK\n" : "NOT OK\n";
        System.out.print(str);

        System.out.print("\nTeste CalculatePageRankEigenVec() - ");
        str = TestCalculatePageRankEigenVec() ? "OK\n" : "NOT OK\n";
        System.out.print(str);

    }

    public static void main(String[] args) throws Exception {
        FileManager.sizeMatriz = 1;
        runAll();

    }
}
