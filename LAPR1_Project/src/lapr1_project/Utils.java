/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1_project;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Scanner;

/**
 * @authors 
 * 
 * LAPR1 2018/2019 
 * Grupo 04 
 * 
 * 1970570 Claudia Monteiro 
 * 1040321 Pedro Barbosa 
 * 1040731 Pedro Sequeira 
 * 1080510 Miguel Silva
 *
 * A classe Utils servirá para realizar todos os cálculos necessários para as
 * funcionalidades principais da aplicação.
 */
public class Utils {
    /**
     * Conta o numero de linhas escritas num ficheiro depois do cabeçalho e sem linhas em branco
     * @param nomeFicheiro - nome do ficheiro a ler
     * @return int numero de linhas do ficheiro
     * @throws IOException 
     */
    public static int contarLinhasFicheiro(String nomeFicheiro) throws IOException {
        Scanner fInput = new Scanner(new File(nomeFicheiro));
        int actualLines = 0;
        boolean startCount = false;
        while (fInput.hasNextLine()) {
            String linha = fInput.nextLine();
            if (linha.trim().length() == 0 && !startCount) {
                startCount = true;
            }
            // Verifica se linha não está em branco
            if ((linha.trim()).length() > 0 && startCount) {
                actualLines++;
            }
        }
        fInput.close();
        return actualLines;
    }
    /**
     * Cria matriz de adjacencias com dados lidos dos ficheiros
     * @param linesNos int número de nós
     * @param nos String[] com informação dos nós
     * @param ramos String[] com informação dos ramos
     * @param linesRamos int número de ramos
     * @return Int[][] matriz de adjacencias
     */
    public static int[][] criarMatriz(int linesNos, String[] nos, String[] ramos, int linesRamos) {
        int matriz[][] = new int[linesNos][linesNos];
        String arrayIds[] = createArrayIds(nos, linesNos);
        int row = -1;
        int column = -1;
        for (int i = 0; i < linesRamos; i++) {
            row = pesquisarElemento(ramos[i].split(Configs.SEPARADOR_DADOS)[0], arrayIds);
            column = pesquisarElemento(ramos[i].split(Configs.SEPARADOR_DADOS)[1], arrayIds);
            if (row == -1 || column == -1) {
                System.out.println("Exitem ramos com ids inválidos - a terminar.");
                System.exit(0);
            }
            // verifica se um vertice não se liga a ele próprio
            if (row != column) {
                // verifica se não existem ligações repetidas
                if (matriz[column][row] == 0) {
                    matriz[column][row] = Integer.parseInt(ramos[i].split(Configs.SEPARADOR_DADOS)[2]);
                    // criar matriz simétrica no caso de um grafo não orientado
                    if (FileManager.netWorkType.equals(Configs.NON_ORIENTED)) {
                        matriz[row][column] = Integer.parseInt(ramos[i].split(Configs.SEPARADOR_DADOS)[2]);
                    }
                } else {
                    System.out.println("Dados repetidos - aplicada a simetria.");
                }
            } else {
                System.out.println("Existem ligações duplicadas - a terminar.");
                System.exit(0);
            }
        }
        return matriz;
    }
    /**
     * Pesquisa um dado elemento num vetor
     * @param valor - elemento que se pretende procurar
     * @param mat - vetor onde se pretende procurar
     * @return int index do elemento encontrado -1 se não encontrado
     */
    public static int pesquisarElemento(String valor, String[] mat) {
        for (int i = 0; i < mat.length; i++) {
            if (mat[i].trim().equalsIgnoreCase(valor)) {
                return i;
            }
        }
        return -1;
    }
    /**
     * Cria vetor com os ids dos nós
     * @param nos String[] vetor com a informação dos nós
     * @param linesNos - número de nós
     * @return String[] vetor com os ids dos nós
     */
    public static String[] createArrayIds(String nos[], int linesNos) {
        String arrayIds[] = new String[linesNos];
        for (int i = 0; i < linesNos; i++) {
            arrayIds[i] = nos[i].split(Configs.SEPARADOR_DADOS)[0];
        }
        return arrayIds;
    }
    /**
     * Copia uma matriz de int para uma matrix de double
     * @param matriz - matriz a copiar
     * @return matriz doble
     */
    public static double[][] copiarMatrizToDouble(int[][] matriz) {
        double[][] result = new double[matriz.length][matriz[0].length];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                result[i][j] = (double) matriz[i][j];
            }
        }
        return result;
    }
    /**
     * Obtem uma coluna da matriz com os valores absolutos
     * @param matriz - matriz bidimensional 
     * @param indexCol - index da coluna que se pretende
     * @param sizeArr - tamanho da matriz[]
     * @return double[] - vator com a coluna da matriz com valores absolutos.
     */
    public static double[] getColunaMatriz(double[][] matriz, int indexCol, int sizeArr) {
        double[] result = new double[sizeArr];

        for (int i = 0; i < sizeArr; i++) {
            result[i] = Math.abs(matriz[i][indexCol]);
        }
        return result;
    }
    /**
     * Multiplica duas matrizes bidimensionais
     * @param A - int[][]
     * @param B - int[][]
     * @return int[][] matriz multiplicada
     */
    public static int[][] multiplyMatrix(int[][] A, int[][] B) {
        int matResult[][] = new int[A.length][B[0].length];
        int tempResult;
        for (int i = 0; i < A.length; i++) {
            tempResult = 0;
            for (int j = 0; j < B[0].length; j++) {
                tempResult = 0;
                for (int b = 0; b < A[0].length; b++) {

                    tempResult = tempResult + A[i][b] * B[b][j];
                    matResult[i][j] = tempResult;
                }
            }
        }
        return matResult;
    }
    /**
     * Cria uma cópia de uma matriz bidimensional
     * @param A - matriz a copiar
     * @return int[][] matriz copiada
     */
    public static int[][] copyMatrix(int[][] A) {
        int matResult[][] = new int[A.length][A[0].length];
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                matResult[i][j] = A[i][j];
            }
        }
        return matResult;
    }
    /**
     * Obtem o index do maior valor da diagonal principal de uma matriz
     * @param mat 
     * @return primeiro index do maior valor encontrado
     */
    public static int calcularIndexMaiorDiagonalPrincipal(double[][] mat) {
        int index = -1;
        double max = 0;
        for (int i = 0; i < mat[0].length; i++) {
            if (mat[i][i] > max) {
                max = mat[i][i];
                index = i;
            }
        }
        return index;
    }
    /**
     * Obtem o index do maior valor de um dado vetor
     * @param vec
     * @return 
     */
    public static int calcularIndexMaiorValorVector(double[] vec) {
        int index = -1;
        double max = 0;
        for (int i = 0; i < vec.length; i++) {
            if (vec[i] > max) {
                max = vec[i];
                index = i;
            }
        }
        return index;
    }
    /**
     * Lista matriz bidimensional
     * @param matriz
     * @param nos 
     */
    public static void listarMatriz(int matriz[][], int nos) {
        String arrayIds[] = createArrayIds(FileManager.nos, FileManager.nos.length);
        for (int a = 0; a < nos; a++) {
            if (a == 0) {
                System.out.printf((String.format("%11s", arrayIds[a])) + " ");
            } else {
                System.out.printf((String.format("%5s", arrayIds[a])) + " ");
            }
        }
        System.out.println("");

        for (int i = 0; i < nos; i++) {
            System.out.printf((String.format("%5s", arrayIds[i])) + " ");
            for (int j = 0; j < nos; j++) {
                System.out.printf((String.format("%5s", matriz[i][j])) + " ");
            }
            System.out.println("");
        }
    }
    /**
     * lista conteudo do vector que tem os dados so ficheiro lido
     * @param conteudo 
     */
    public static void listarConteudoFicheiro(String[] conteudo) {  
        for (int i = 0; i < conteudo.length; i++) {
            System.out.println(conteudo[i]);
        }

    }
    /**
     * calcula a magnitude de um vetor
     * @param arr
     * @return 
     */
    public static double magnitude(double arr[]) {
        double magnitude = 0;
        if (arr != null) {
            if (arr.length != 0) {
                double somaValQuadrado = 0;
                for (int i = 0; i < arr.length; i++) {
                    somaValQuadrado += Math.pow(arr[i], 2);
                }
                magnitude = Math.sqrt(somaValQuadrado);
            }
        }
        return magnitude;
    }
    /**
     * normaliza um vetor dado
     * @param arr
     * @return 
     */
    public static double[] nomalizar(double arr[]) {
        double res[] = null;
        if (arr != null) {
            res = new double[arr.length];
            if (arr.length != 0) {
                double mag = magnitude(arr);
                if (mag != 0.0) {
                    for (int i = 0; i < arr.length; i++) {
                        res[i] = arr[i] / mag;
                    }
                }
            }
        }
        if (res == null) {
            res = new double[0];
        }
        return res;
    }
    /**
     * verifica se vetor esta normalizado
     * @param arr
     * @return 
     */
    public static boolean isNormalizado(double arr[]) {
        double erro = 1e-14;
        double magnitude = magnitude(arr);
        return Math.abs(1 - magnitude) < erro;
    }
    /**
     * obtem o url de um dado index do vetor (com dados dos nós)
     * @param index
     * @param mat
     * @return 
     */
    public static String getUrlForNode(int index, String[] mat) {
        String url = null;
        url = mat[index].split(Configs.SEPARADOR_DADOS)[4];
        return url;
    }
    /**
     * Abre pagina web do dado url no browser default
     * @param url 
     */
    public static void openPage(String url) {
        try {
            Desktop desktop = java.awt.Desktop.getDesktop();
            URI oURL = new URI(url.trim());
            desktop.browse(oURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * soma duas matrizes bidibensionais
     * @param matA
     * @param matB
     * @return 
     */
    public static double[][] matrixSum(double[][] matA, double[][] matB) {
        int dimention = matA.length;
        double[][] sum = new double[dimention][dimention];
        for (int i = 0; i < dimention; i++) {
            for (int j = 0; j < dimention; j++) {
                sum[i][j] = matA[i][j] + matB[i][j];
            }
        }
        return sum;
    }
    /**
     * mutiplica uma dada matriz por um escalar
     * @param mat
     * @param scalar
     * @return 
     */
    public static double[][] matrixTimesScalar(double[][] mat, double scalar) {
        int dimention = mat.length;
        double[][] times = new double[dimention][dimention];
        for (int i = 0; i < dimention; i++) {
            for (int j = 0; j < dimention; j++) {
                times[i][j] = mat[i][j] * scalar;
            }
        }
        return times;
    }
    /**
     * Cria matriz bidimensional com valores dados
     * @param value
     * @param dimention
     * @return 
     */
    public static double[][] fillMatrixNxN(double value, int dimention) {
        double[][] matrix = new double[dimention][dimention];
        for (int i = 0; i < dimention; i++) {
            for (int j = 0; j < dimention; j++) {
                matrix[i][j] = value;
            }
        }
        return matrix;
    }
    /**
     * cria vector com valores dados
     * @param value
     * @param dimention
     * @return 
     */
    public static double[] fillArray(double value, int dimention) {
        double[] vector = new double[dimention];
        for (int i = 0; i < dimention; i++) {
            vector[i] = value;
        }
        return vector;
    }
    /**
     * multiplica matriz bidimensional por vetor
     * @param matrix
     * @param vector
     * @return 
     */
    public static double[] multiplyMatrixVector(double[][] matrix, double[] vector) {
        int rows = matrix.length;
        int columns = vector.length;

        if (rows != columns) {
            throw new IllegalArgumentException("Ao multiplicar matrizes o número linhas de A tem de ser igual ao número colunas de B.");
        }

        double[] result = new double[rows];

        for (int row = 0; row < rows; row++) {
            double sum = 0;
            for (int column = 0; column < columns; column++) {
                sum += matrix[row][column] * vector[column];
            }
            result[row] = sum;
        }
        return result;
    }
    /**
     * imprime dados do vetor PageRank obtido pelo metodo do vetor próprio
     * @param arrResul 
     */
    public static void printPageRankEigenVec(double[] arrResul) {
        if (arrResul.length == 0) {
            System.out.println("Não foi possivel encontrar um valor próprio maior que 0 para calcular o Page Rank.");
        } else {
            for (int i = 0; i < arrResul.length; i++) {
                System.out.printf("%s%s%s%.3f\n", "O Page Rank de ",
                        FileManager.nos[i].split(Configs.SEPARADOR_DADOS)[1],
                        " é ",
                        arrResul[i]);
            }
        }
        System.out.println();
    }
    /**
     * imprime dados do vetor PageRank e suas iterações
     * @param arrResul 
     */
    public static void printPageRankIteration(double[][] arrResul) {
        if (arrResul.length == 0) {
            System.out.println("Não foi possivel encontrar um valor próprio maior que 0 para calcular o Page Rank.");
        } else {
            // imprimir nome dos nós
            System.out.printf("%1$4s %2$" + String.valueOf(arrResul.length - 1).length() + "s ", "", "");
            for (int i = 0; i < FileManager.nos.length; i++) {
                System.out.printf("%1$6s ", FileManager.nos[i].split(Configs.SEPARADOR_DADOS)[0]);
            }
            System.out.println();
            for (int i = 0; i < arrResul.length; i++) {
                System.out.printf("%s %2$" + String.valueOf(arrResul.length - 1).length() + "s ", "Ite.", i);
                for (int j = 0; j < arrResul[0].length; j++) {
                    System.out.printf("% 2.3f ", arrResul[i][j]);
                }
                System.out.println();
            }
        }
        System.out.println();
    }
    /**
     * verifica de é inteiro
     * @param str
     * @return 
     */
    public static boolean isInt(String str) {
        try {
            int i= Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
