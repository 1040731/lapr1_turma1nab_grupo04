/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1_project;

import java.io.IOException;

/**
 * @authors 
 * 
 * LAPR1 2018/2019 
 * Grupo 04 
 * 
 * 1970570 Claudia Monteiro 
 * 1040321 Pedro Barbosa 
 * 1040731 Pedro Sequeira 
 * 1080510 Miguel Silva
 *
 * Esta classe principal servirá apenas para arrancar a aplicação.
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        //execução do programa
        //Onde se encontra a validar os parametros de entrada.
        if (validaParamEntrada(args)) {
            String nomeFNos = "";
            String nomeFRamos = "";
            if (args.length == Configs.N_PARAM_ENTRADA_MENU) {
                execMenu(args, nomeFNos, nomeFRamos);
            }
            if (args.length == Configs.N_PARAM_ENTRADA_FICH) {
                execFicheiroNon(args, nomeFNos, nomeFRamos);
            }
            if(args.length == Configs.N_PARAM_ENTRADA_FICH_ORIENTED){
                execFicheiroOriented(args, nomeFNos, nomeFRamos);
            }

        } else {
            System.out.println("Parâmetros errados! Deve efetuar da seguinte forma:");
            System.out.println("java -jar <nome app> <-n> <nome_Ficheiro_Nos.csv> <nome_Ficheiro_Ramos.csv>");
            System.out.println("ou para redes não orientadas");
            System.out.println("java -jar <nome app> <-t> <-k> "
                    + "<int potencia> <nome_Ficheiro_Nos.csv> <nome_Ficheiro_Ramos.csv>");
            System.out.println("ou para redes orientadas");
            System.out.println("java -jar <nome app> <-t> <-k> "
                    + "<int potencia> <-d> <double damping> <nome_Ficheiro_Nos.csv> <nome_Ficheiro_Ramos.csv>");
            
        }
    }
    
    /**
     *
     * @param args
     * @param nomeFNos
     * @param nomeFRamos
     * @throws IOException
     */
    public static void execMenu(String args[], String nomeFNos, String nomeFRamos) throws IOException {
        for (int i = 0; i < args.length; i++) {
            if (i != Utils.pesquisarElemento("-n", args)) {
                if (validaNomeFNos(args[i])) {
                    nomeFNos = args[i];
                }
                if (validaNomeFRamos(args[i])) {
                    nomeFRamos = args[i];
                }
            }
        }
        if (!nomeFNos.equals("") && !nomeFRamos.equals("")) {
            FileManager.lerFicheirosNosRamos(nomeFNos, nomeFRamos);
            if (Configs.NON_ORIENTED.equals(FileManager.netWorkType)) {
                Menu.executarMenuPrincipal(SNA.matriz);
            } else if (Configs.ORIENTED.equals(FileManager.netWorkType)) {
                Menu.executarMenuPrincipalOrientado(SNA.matriz);
            }
        } else {
            System.out.println("Nomes dos ficheiros inválidos!");
        }
    }
    
    /**
     *
     * @param args
     * @param nomeFNos
     * @param nomeFRamos
     * @throws IOException
     */
    public static void execFicheiroNon(String args[], String nomeFNos, String nomeFRamos) throws IOException {
        int k = Utils.pesquisarElemento("-k", args);
        for (int i = 0; i < args.length; i++) {
            if (i != Utils.pesquisarElemento("-t", args) && i != k && i != k + 1) {
                if (validaNomeFNos(args[i])) {
                    nomeFNos = args[i];
                }
                if (validaNomeFRamos(args[i])) {
                    nomeFRamos = args[i];
                }
            }
        }
        if (!nomeFNos.equals("") && !nomeFRamos.equals("")) {
            FileManager.lerFicheirosNosRamos(nomeFNos, nomeFRamos);
            if(FileManager.netWorkType.equals(Configs.NON_ORIENTED)){
                SNA.calculateAll(SNA.matriz, FileManager.sizeMatriz, Integer.parseInt(args[k + 1]));
                System.out.println("Todos os cálculos foram efetuados - verifique ficheiro de saida");
            }else{
                System.out.println("Os ficheiros apresentados são de redes orientadas.");
                System.out.println("Necessita do parametro <-d> para a sua execução - a terminar");
                System.exit(0);
            }
        } else {
            System.out.println("Nomes dos ficheiros inválidos");
        }
    }
    
    /**
     *
     * @param args
     * @param nomeFNos
     * @param nomeFRamos
     * @throws IOException
     */
    public static void execFicheiroOriented(String args[], String nomeFNos, String nomeFRamos) throws IOException{
        int k = Utils.pesquisarElemento("-k", args);
        int d = Utils.pesquisarElemento("-d", args);
        for (int i = 0; i < args.length; i++) {
            if (i != Utils.pesquisarElemento("-t", args) && i != k && i!=k+1 && i!=d && i!=d+1) {
                if (validaNomeFNos(args[i])) {
                    nomeFNos = args[i];
                }
                if (validaNomeFRamos(args[i])) {
                    nomeFRamos = args[i];
                }
            }
        }
        if (!nomeFNos.equals("") && !nomeFRamos.equals("")) {
            FileManager.lerFicheirosNosRamos(nomeFNos, nomeFRamos);
            if(FileManager.netWorkType.equals(Configs.ORIENTED)){
                SNA.calculateAllOriented(SNA.matriz, 
                    FileManager.sizeMatriz, 
                    Integer.parseInt(args[k + 1])+1, 
                    Double.parseDouble(args[d+1])
                );
                System.out.println("Todos os cálculos foram efetuados - verifique ficheiro de saida"); 
            }else{
                System.out.println("Os ficheiros apresentados são de redes não orientadas.");
                System.out.println("Não necessita do parametro <-d> para a sua execução - a terminar");
                System.exit(0);
            }
        } else {
            System.out.println("Nomes dos ficheiros inválidos");
        }
    }

    /**
     * Valida os parametros de entrada da linha de comandos
     * @param args
     * @return 
     */
    private static boolean validaParamEntrada(String[] args) {
        boolean isValid = false;
        int a;
        int k;
        int d;
            if(Utils.pesquisarElemento("-n", args) !=-1 && args.length != Configs.N_PARAM_ENTRADA_MENU){
                return isValid;
            }
            if( (a = Utils.pesquisarElemento("-n", args) ) != -1){
                boolean validName = true;
                for(int i=0; i< args.length && validName; i++){
                    if(i!=a){
                        validName = validaNomeF(args[i]);
                    }
                }
                isValid = validName;
                return isValid;
            }
            if(Utils.pesquisarElemento("-k", args) ==-1){
                return isValid;
            }
            if(Utils.pesquisarElemento("-d", args) !=-1 && args.length != Configs.N_PARAM_ENTRADA_FICH_ORIENTED){
                return isValid;
            }   
            if ((a=Utils.pesquisarElemento("-t", args)) != -1 && 
            (k=Utils.pesquisarElemento("-k", args)) != -1 && 
            Utils.pesquisarElemento("-d", args) == -1) {
                boolean validName = true;
                boolean validK = false;
                for(int i=0; i<args.length && validName; i++){
                    if(i!=a && i!=k && i!=k+1){
                        validName = validaNomeF(args[i]);
                    }
                    if(Utils.isInt(args[k+1]) && Integer.parseInt(args[k+1]) > 0){
                        validK = true;
                    }
                    isValid = validName && validK;
                } 
            }
            if ((a=Utils.pesquisarElemento("-t", args)) != -1 && 
            (k=Utils.pesquisarElemento("-k", args)) != -1 && 
            (d=Utils.pesquisarElemento("-d", args)) != -1) {
                boolean validName = true;
                boolean validK = false;
                boolean validD = false;
                for(int i=0; i<args.length && validName; i++){
                    if(i!=a && i!=k && i!=k+1 && i!=d && i!=d+1){
                        validName = validaNomeF(args[i]);
                    }
                    if(Utils.isInt(args[k+1]) && Integer.parseInt(args[k+1]) > 0){
                        validK = true;
                    }
                    if(isDouble(args[d+1]) && Double.parseDouble(args[d+1]) > 0  && Double.parseDouble(args[d+1]) <=1){
                        validD= true;
                    }
                    isValid = validName && validK && validD;
                } 
            }
        return isValid;
    }
   
    private static boolean isDouble(String str) {
        try {
            double i= Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
    
    private static boolean validaNomeF(String NomeF){
        boolean isValid = false;
        String [] campos = NomeF.split(Configs.SEPARADOR_NOME_FICH);
        int valor = campos.length;
        String extensao = campos[2].split("[.]")[1];
        if(valor == Configs.CAMPOS_NOME_FICH && extensao.equals(Configs.EXTENSAO_FICH) ){
            isValid = true;
        }
        return isValid;
    }
    
    private static boolean validaNomeFNos(String NomeFNos){
        boolean isValid = false;
        String [] campos = NomeFNos.split(Configs.SEPARADOR_NOME_FICH);
        int valor = campos.length;
        String tipo = campos[2].split("[.]")[0];
        String extensao = campos[2].split("[.]")[1];
        if(valor == Configs.CAMPOS_NOME_FICH && extensao.equals(Configs.EXTENSAO_FICH) ){
            if(tipo.equals("nos")){
                isValid = true;
            }
        }
        return isValid;
    }
    private static boolean validaNomeFRamos(String NomeFRamos){
        boolean isValid = false;
        String [] campos = NomeFRamos.split(Configs.SEPARADOR_NOME_FICH);
        int valor = campos.length;
        String tipo = campos[2].split("[.]")[0];
        String extensao = campos[2].split("[.]")[1];
        if(valor == Configs.CAMPOS_NOME_FICH && extensao.equals(Configs.EXTENSAO_FICH)){
            if(tipo.equals("ramos")){
                isValid = true;
            }
        }
        return isValid;
    }
    
}
