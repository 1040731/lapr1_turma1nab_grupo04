/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1_project;

/**
 * @authors 
 * 
 * LAPR1 2018/2019 
 * Grupo 04 
 * 
 * 1970570 Claudia Monteiro 
 * 1040321 Pedro Barbosa 
 * 1040731 Pedro Sequeira 
 * 1080510 Miguel Silva
 *
 * Classe de configurações para definição de constantes.
 */
public class Configs {
   public static final String SEPARADOR_DADOS = ",";
   public static final int N_PARAM_ENTRADA_MENU = 3;
   public static final int N_PARAM_ENTRADA_FICH = 5;
   public static final int N_PARAM_ENTRADA_FICH_ORIENTED = 7;
   public static final String SEPARADOR_NOME_FICH = "_";
   public static final int CAMPOS_NOME_FICH = 3;
   public static final String EXTENSAO_FICH = "csv";
   public static final int N_CAMPOS_FICH_NOS = 5;
   public static final int N_CAMPOS_FICH_RAMOS = 3;
   public static final int VALOR_MAX_NOS = 200;
   public static final String ORIENTED = "oriented";
   public static final String NON_ORIENTED = "nonoriented";
   
}
