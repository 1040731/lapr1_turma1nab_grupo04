/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testes;

import java.io.IOException;
import lapr1_project.Configs;
import lapr1_project.FileManager;
import lapr1_project.Utils;

/**
 * @authors 
 * 
 * LAPR1 2018/2019 
 * Grupo 04 
 * 
 * 1970570 Claudia Monteiro 
 * 1040321 Pedro Barbosa 
 * 1040731 Pedro Sequeira 
 * 1080510 Miguel Silva
 *
 * Classe de testes da classe Utils.
 */
public class UtilsTest {

    private static double arrDoubleTest[] = new double[]{1.0, 2.0, 1.0, 3.0};

    private static double matDoubleTest[][] = new double[][]{
        {1.0, 2.0, 1.0, 1.0},
        {1.0, 2.0, 1.0, 2.0},
        {1.0, 2.0, 1.0, 1.0},
        {1.0, 2.0, 1.0, 3.0},};
    
    private static double arrDoubleNorma[] = new double[]{0.6,0.8};
    
    private static double arrTest[] = new double[]{3.0,4.0};
    
    private static int matIntTest[][] = new int[][]{
        {1, 2, 1, 1},
        {1, 2, 1, 2},
        {1, 2, 1, 1},
        {1, 2, 1, 3},};

    private static int A[][] = {
        {0, 1, 1, 0, 0, 1, 0},
        {1, 0, 0, 0, 0, 0, 0},
        {1, 0, 0, 1, 1, 1, 0},
        {0, 0, 1, 0, 1, 0, 1},
        {0, 0, 1, 1, 0, 0, 0},
        {1, 0, 1, 0, 0, 0, 1},
        {0, 0, 0, 1, 0, 1, 0},};

    private static boolean compararVecDouble(double arr1[], double arr2[]) {
        boolean flag = true;
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                flag = false;
            }
        }
        return flag;
    }
    
    private static boolean compararVecDouble(double arr1[], double val){
        boolean flag = true;
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != val) {
                flag = false;
            }
        }
        return flag;
    }
    
    private  static boolean compararMatDouble(double mat1[][], double mat2[][]){
        boolean flag = true;
        for(int i = 0; i<mat1.length; i++){
            for(int j = 0 ; j<mat1[i].length; j++){
                 if(mat1[i][j] != mat2[i][j]){
                     flag = false;
                 }   
            }
        }
        return flag;
    }
    
    private  static boolean compararMatDouble(double mat1[][],double val ){
        boolean flag = true;
        for(int i = 0; i<mat1.length; i++){
            for(int j = 0 ; j<mat1[i].length; j++){
                 if(mat1[i][j] != val){
                     flag = false;
                 }   
            }
        }
        return flag;
    }

    /**
     * Test of contarLinhasFicheiro method, of class Utils.
     */
    public static boolean testContarLinhasFicheiro() throws IOException {
        String nomeFicheiro = "contagemLinhas.txt";
        int expResult = 17;
        int result = Utils.contarLinhasFicheiro(nomeFicheiro);
        
        return expResult == result;
    }

    /**
     * Test of criarMatriz method, of class Utils.
     */
    public static boolean testCriarMatriz() {
        int linesNos = 3;
        String[] nos = {"s01","s02","s03"};
        String[] ramos = {"s01,s02,1", "s01,s03,1", "s02,s03,1"};
        FileManager.netWorkType = Configs.NON_ORIENTED;
        int linesRamos = 3;
        boolean res = true;
        int[][] expResult = {
            {0,1,1},
            {1,0,1},
            {1,1,0}
        };
        
        int[][] result = Utils.criarMatriz(linesNos, nos, ramos, linesRamos);
        for(int i =0; i<result.length; i++){
            for(int j = 0; j<result[0].length; j++){
                if(expResult[i][j] != result[i][j]){
                    res = false;
                }
            }
        }
        return res;
    }

  

    /**
     * Test of CopiarMatrizToDouble method, of class Utils.
     */
    public static boolean testCopiarMatrizToDouble() {    
        boolean flag = false;
        //double[][] expResult = null;
        double[][] result = Utils.copiarMatrizToDouble(matIntTest);
        //comparar mat double
        if(result.length == matDoubleTest.length && result[0].length == matDoubleTest[0].length){
            flag = compararMatDouble(result, matDoubleTest);
        }
        return flag;
    }

    /**
     * Test of getColunaMatriz method, of class Utils.
     */
    public static boolean testGetColunaMatriz() {
        double[] expected = arrDoubleTest;

        double[] result = Utils.getColunaMatriz(matDoubleTest, 3, expected.length);

        return compararVecDouble(expected, result);
    }


    /**
     * Test of CalcularIndexMaiorDiagonalPrincipal method, of class Utils.
     */
    public static boolean  testCalcularIndexMaiorDiagonalPrincipal() {
       
        double result = Utils.calcularIndexMaiorDiagonalPrincipal(matDoubleTest);
        double expResult = 3.0;
      
    
     return result == expResult;   
        
    }

    /**
     * Test of multiplyMatrix method, of class Utils.
     */
    public static boolean testMultiplyMatrix() {

        boolean bol = true;

        int B[][] = {
            {0, 1, 1, 0, 0, 1, 0},
            {1, 0, 0, 0, 0, 0, 0},
            {1, 0, 0, 1, 1, 1, 0},
            {0, 0, 1, 0, 1, 0, 1},
            {0, 0, 1, 1, 0, 0, 0},
            {1, 0, 1, 0, 0, 0, 1},
            {0, 0, 0, 1, 0, 1, 0},};

        int matResult[][] = {
            {3, 0, 1, 1, 1, 1, 1},
            {0, 1, 1, 0, 0, 1, 0},
            {1, 1, 4, 1, 1, 1, 2},
            {1, 0, 1, 3, 1, 2, 0},
            {1, 0, 1, 1, 2, 1, 1},
            {1, 1, 1, 2, 1, 3, 0},
            {1, 0, 2, 0, 1, 0, 2},};

        int[][] matTest = Utils.multiplyMatrix(A, B);

        for (int i = 0; i < matTest.length; i++) {
            for (int j = 0; j < matTest[0].length; j++) {

                if (matResult[i][j] != matTest[i][j]) {

                    bol = false;
                }
            }
        }

        return bol;
    }
    
    public static boolean testIndexMaiorValorVector(){
        boolean bol = false;
        
        double expected = 3.0;
        double res = Utils.calcularIndexMaiorValorVector(UtilsTest.arrDoubleTest);
        
        if(res == expected){
            bol = true;
        }
        
        return bol;
    }

    /**
     * Test of copyMatrix method, of class Utils.
     */
    public static boolean testCopyMatrix() {

        boolean bol = true;
        int matResult[][] = {
            {0, 1, 1, 0, 0, 1, 0},
            {1, 0, 0, 0, 0, 0, 0},
            {1, 0, 0, 1, 1, 1, 0},
            {0, 0, 1, 0, 1, 0, 1},
            {0, 0, 1, 1, 0, 0, 0},
            {1, 0, 1, 0, 0, 0, 1},
            {0, 0, 0, 1, 0, 1, 0},};

        int[][] matTest = Utils.copyMatrix(A);

        for (int i = 0; i < matTest.length; i++) {
            for (int j = 0; j < matTest[0].length; j++) {

                if (matResult[i][j] != matTest[i][j]) {

                bol = false;
                    
                }
            }
        }

        return bol;
    }
    
    public static boolean magnitudeTest() {
        double resultEsperado = 5;
        
        double result = Utils.magnitude(arrTest);
        return result == resultEsperado;
    }

    public static boolean normalizarTest() {
        
        double result[] = Utils.nomalizar(arrTest);
       
        return compararVecDouble(result, arrDoubleNorma);
    }
    
    public static boolean isNormalizadoTest(){ 
        return Utils.isNormalizado(arrDoubleNorma);
    }
    
    public static boolean testMatrixSum(){
         double[][] expectedRes = new double[][]{
            {2.0, 4.0, 2.0, 2.0},
            {2.0, 4.0, 2.0, 4.0},
            {2.0, 4.0, 2.0, 2.0},
            {2.0, 4.0, 2.0, 6.0},};

        double[][] res = Utils.matrixSum(matDoubleTest, matDoubleTest);
 
        return compararMatDouble(res, expectedRes);
    }
    
    public static boolean testMatrixTimesScalar(){
         double[][] expectedRes = new double[][]{
            {2.0, 4.0, 2.0, 2.0},
            {2.0, 4.0, 2.0, 4.0},
            {2.0, 4.0, 2.0, 2.0},
            {2.0, 4.0, 2.0, 6.0},};

        double[][] res = Utils.matrixTimesScalar(matDoubleTest, 2);
        
        return compararMatDouble(res, expectedRes);
    }
    
     public static boolean testFillMatrixNxN(){
        double val=2.0;
        int dimention=2;
        double[][] res = Utils.fillMatrixNxN(val, dimention);
   
        return compararMatDouble(res, val);
    }
     
      public static boolean testFillArray(){
        double val=2.0;
        int dimention=2;
        double[] res = Utils.fillArray(val, dimention);
   
        return compararVecDouble(res, val);
    }
    
    public static boolean testMultiplyMatrixVector(){
         double[] expectedRes = new double[]{9.0, 12.0, 9.0, 15.0};

        double[] res = Utils.multiplyMatrixVector(matDoubleTest,arrDoubleTest);
       
        return compararVecDouble(res, expectedRes);
    }
    
    public static void runAll() throws IOException{
        String str = "";
        
        System.out.print("\nTeste MultiplyMatrix() - ");
        str = testMultiplyMatrix()? "OK\n": "NOT OK\n";
        System.out.print(str);
        
        System.out.print("\nTeste testIndexMaiorValorVector() - ");
        str = testIndexMaiorValorVector()? "OK\n": "NOT OK\n";
        System.out.print(str);
                
        System.out.print("\nTeste GetColunaMatriz() - ");
        str = testGetColunaMatriz()? "OK\n": "NOT OK\n";
        System.out.print(str);
        
        System.out.print("\nTeste CriarMatriz() - ");
        str = testCriarMatriz()? "OK\n": "NOT OK\n";
        System.out.print(str);
        
        System.out.print("\nTeste CopyMatrix() - ");
        str = testCopyMatrix()? "OK\n": "NOT OK\n";
        System.out.print(str);
               
        System.out.print("\nTeste CopiarMatrizToDouble() - ");
        str = testCopiarMatrizToDouble()? "OK\n": "NOT OK\n";
        System.out.print(str);
        
        System.out.print("\nTeste ContarLinhasFicheiro() - ");
        str = testContarLinhasFicheiro()? "OK\n": "NOT OK\n";
        System.out.print(str);
        
        System.out.print("\nTeste CalcularIndexMaiorDiagonalPrincipal() - ");
        str = testCalcularIndexMaiorDiagonalPrincipal()? "OK\n": "NOT OK\n";     
        System.out.print(str);
        
        System.out.print("\nTeste magnitude() - ");
        str = magnitudeTest()? "OK\n": "NOT OK\n";     
        System.out.print(str);
        
        System.out.print("\nTeste normalizar() - ");
        str = normalizarTest()? "OK\n": "NOT OK\n";     
        System.out.print(str);
        
        System.out.print("\nTeste isNormalizado() - ");
        str = isNormalizadoTest()? "OK\n": "NOT OK\n";     
        System.out.print(str);
     
        System.out.print("\nTeste matrixSum() - ");
        str = testMatrixSum()? "OK\n": "NOT OK\n";     
        System.out.print(str);
        
        System.out.print("\nTeste MatrixTimesScalar() - ");
        str = testMatrixTimesScalar()? "OK\n": "NOT OK\n";     
        System.out.print(str);
        
        System.out.print("\nTeste fillMatrixNxN() - ");
        str = testFillMatrixNxN()? "OK\n": "NOT OK\n";     
        System.out.print(str);
        
        System.out.print("\nTeste FillArray() - ");
        str = testFillArray()? "OK\n": "NOT OK\n";     
        System.out.print(str);
        
        System.out.print("\nTeste MultiplyMatrixVector() - ");
        str = testMultiplyMatrixVector()? "OK\n": "NOT OK\n";     
        System.out.print(str);
    }
    
    public static void main(String[] args) throws IOException, Exception {
        runAll();
    }

}
