/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1_project;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * @authors 
 * 
 * LAPR1 2018/2019 
 * Grupo 04 
 * 
 * 1970570 Claudia Monteiro 
 * 1040321 Pedro Barbosa 
 * 1040731 Pedro Sequeira 
 * 1080510 Miguel Silva
 *
 * Classe utilizada para gerir a leitura e escrita nos ficheiros de input e
 * output de informação da aplicação.
 */
public class FileManager {

    public static int sizeMatriz = -1;
    public static String nos[];
    public static String ramos[];
    public static String nomeRede;
    public static BufferedWriter writer;
    public static String netWorkType;

    /**
     * Gera a matriz de ramos e nos da rede social lida dos ficheiros
     *
     * @param nomeFNos - nome do ficheiro de nos
     * @param nomeFRamos - nome do ficheiro de ramos.
     * @throws IOException
     */
    public static void lerFicheirosNosRamos(String nomeFNos, String nomeFRamos) throws IOException {
        validaFicheiros(nomeFNos, nomeFRamos);
        Scanner fNos = new Scanner(new File(nomeFNos));
        Scanner fRamos = new Scanner(new File(nomeFRamos));
        nomeRede = nomeFNos.split(Configs.SEPARADOR_NOME_FICH)[1];
        int sizeNos = Utils.contarLinhasFicheiro(nomeFNos);
        if (sizeNos > Configs.VALOR_MAX_NOS) {
            System.out.println("O valor max de nos é " + Configs.VALOR_MAX_NOS);
            System.exit(0);
        }
        int sizeRamos = Utils.contarLinhasFicheiro(nomeFRamos);
        nos = new String[sizeNos];
        ramos = new String[sizeRamos];
        int linesN = 0;
        boolean startNos = false;
        while (fNos.hasNextLine()) {
            String linha = fNos.nextLine();
            if (!startNos && linha.trim().length() == 0) {
                startNos = true;
            }
            // Verifica se linha não está em branco e tem os 5 campos
            if ((linha.trim()).length() > 0
                    && linha.split(Configs.SEPARADOR_DADOS).length == Configs.N_CAMPOS_FICH_NOS
                    && startNos) {
                nos[linesN] = linha;
                linesN++;
            }
        }
        fNos.close();

        int linesR = 0;
        boolean startRamos = false;
        while (fRamos.hasNextLine()) {
            String linha = fRamos.nextLine();
            if (!startRamos && linha.trim().length() == 0) {
                startRamos = true;
            }
            // Verifica se linha não está em branco e tem os 3 campos
            if ((linha.trim()).length() > 0
                    && linha.split(Configs.SEPARADOR_DADOS).length == Configs.N_CAMPOS_FICH_RAMOS
                    && startRamos) {
                ramos[linesR] = linha;
                linesR++;
            }
        }
        fRamos.close();
        sizeMatriz = sizeNos;
        SNA.matriz = Utils.criarMatriz(sizeNos, nos, ramos, sizeRamos);
    }
    /**
     * guarda todos os dados em ficheiro (rede não orientada)
     * @param nodeDegrees
     * @param eigenVec
     * @param averageDegree
     * @param density
     * @param matriz
     * @param potencia
     * @throws IOException 
     */
    public static void guardarEmFicheiro(
            int[] nodeDegrees,
            double[] eigenVec,
            double averageDegree,
            double density,
            int[][] matriz,
            int potencia) throws IOException {

        Date date = new Date();
        String dataFormatada = new SimpleDateFormat("yyyy-MM-dd").format(date);
        String nomeFicheiro = "out_" + nomeRede + "_" + dataFormatada;
        writer = new BufferedWriter(new FileWriter(nomeFicheiro + ".txt"));
        guardarConteudoFicheiros(FileManager.nos, FileManager.ramos);
        guardarNodeDegrees(nodeDegrees, writer);
        guardarEigenVec(eigenVec, writer);
        guardarAverageDegree(averageDegree, writer);
        guaradarDensity(density, writer);
        guardarPowerAM(matriz, writer, potencia);
        writer.flush();
        writer.close();
    }
    /**
     * guarda todos os dados em ficheiro (rede orientada)
     * @param inDegree
     * @param outDegree
     * @param pageRankeigenVec
     * @param pageRank
     * @param matriz
     * @param iteracoes
     * @param damp
     * @throws IOException 
     */
    public static void guardarEmFicheiroOiented(
            int[] inDegree,
            int[] outDegree,
            double[] pageRankeigenVec,
            double pageRank[][],
            int matriz[][],
            int iteracoes,
            double damp) throws IOException {

        Date date = new Date();
        String dataFormatada = new SimpleDateFormat("yyyy-MM-dd").format(date);
        String nomeFicheiro = "out_" + nomeRede + "_" + dataFormatada;
        writer = new BufferedWriter(new FileWriter(nomeFicheiro + ".txt"));
        guardarConteudoFicheiros(FileManager.nos, FileManager.ramos);
        guardarInDegrees(inDegree, writer);
        guardarOutDegrees(outDegree, writer);
        guardarPageRankEigenVec(pageRankeigenVec, writer,damp);
        guardarPageRank(pageRank, writer, damp);
        writer.flush();
        writer.close();
    }

    private static void guardarConteudoFicheiros(String[] nos, String[] ramos) throws IOException {
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
        writer.write("=================== Tipo de Rede = " + FileManager.netWorkType + " ======================");
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
        writer.write("=========================== Ficheiro NOS =========================");
        writer.newLine();
        for (int i = 0; i < nos.length; i++) {
            writer.write(String.format("%s\n", nos[i]));
        }
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
        writer.write("========================== Ficheiro RAMOS ========================");
        writer.newLine();
        for (int i = 0; i < ramos.length; i++) {
            writer.write(String.format("%s\n", ramos[i]));
        }
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
    }

    private static void guardarNodeDegrees(int[] nodeDegrees, BufferedWriter writer) throws IOException {
        writer.write("=========================== Node Degree ==========================");
        writer.newLine();
        for (int i = 0; i < nodeDegrees.length; i++) {
            writer.write(String.format("%s%s%s%d%s\n",
                    "O Grau do nó de ",
                    nos[i].split(Configs.SEPARADOR_DADOS)[1],
                    " é ",
                    nodeDegrees[i],
                    ".")
            );
        }
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
    }

    private static void guardarEigenVec(double[] eigenVec, BufferedWriter writer) throws IOException {
        writer.write("=========================== Eigencector Centrality =========================");
        writer.newLine();
        for (int i = 0; i < eigenVec.length; i++) {
            writer.write(String.format("%s%s%s%.3f%s\n",
                    "A centralidade de ",
                    nos[i].split(Configs.SEPARADOR_DADOS)[1],
                    " é ",
                    eigenVec[i],
                    ".")
            );
        }
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
    }

    private static void guardarAverageDegree(double averageDegree, BufferedWriter writer) throws IOException {
        writer.write("========================== Average Degree ========================");
        writer.newLine();
        writer.write(String.format("%s%.3f", "A média dos graus de todos os nós da rede é ", averageDegree));
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
    }

    private static void guaradarDensity(double density, BufferedWriter writer) throws IOException {
        writer.write("=========================== Density ==============================");
        writer.newLine();
        writer.write(String.format("%s%.3f", "O valor do nível geral de conectividade da rede é ", density));
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
    }

    private static void guardarPowerAM(int[][] matriz, BufferedWriter writer, int potencia) throws IOException {
        for (int p = 1; p <= potencia; p++) {
            writer.newLine();
            writer.write("========================== PowerAM k=" + p + " ===========================");
            writer.newLine();
            writer.newLine();
            int[][] powerAM = SNA.calculatePowersAM(p, matriz);
            for (int j = 0; j < powerAM[0].length; j++) {
                if (j == 0) {
                    writer.write(String.format("%9s", nos[j].split(Configs.SEPARADOR_DADOS)[0]));
                } else {
                    writer.write(String.format("%5s", nos[j].split(Configs.SEPARADOR_DADOS)[0]));
                }
            }
            writer.newLine();
            for (int i = 0; i < powerAM.length; i++) {
                writer.write(String.format("%s", nos[i].split(Configs.SEPARADOR_DADOS)[0]));
                for (int a = 0; a < powerAM[0].length; a++) {
                    writer.write(String.format("%5d", powerAM[i][a]));
                }
                writer.newLine();
            }
        }
        writer.newLine();
        writer.write("================================================================");
        writer.newLine();
    }

    private static void guardarInDegrees(int[] inDegree, BufferedWriter writer) throws IOException {
        writer.write("============================ In Degree ===========================");
        writer.newLine();
        for (int i = 0; i < inDegree.length; i++) {
            writer.write(String.format("%s%s%s%d%s\n",
                    "O Grau de entrada do nó de ",
                    nos[i].split(Configs.SEPARADOR_DADOS)[1],
                    " é ",
                    inDegree[i],
                    ".")
            );
        }
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
    }

    private static void guardarOutDegrees(int[] outDegree, BufferedWriter writer) throws IOException {
        writer.write("=========================== Out Degree ===========================");
        writer.newLine();
        for (int i = 0; i < outDegree.length; i++) {
            writer.write(String.format("%s%s%s%d%s\n",
                    "O Grau de saída do nó de ",
                    nos[i].split(Configs.SEPARADOR_DADOS)[1],
                    " é ",
                    outDegree[i],
                    ".")
            );
        }
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
    }

    private static void guardarPageRankEigenVec(double[] PageRankEigenVec, BufferedWriter writer, double damp) throws IOException {
        writer.write("===================== PageRank (Vector Próprio) ==================");
        writer.newLine();
        writer.write(String.format("====================== Damping factor = %.2f =====================\n", damp));
        for (int i = 0; i < PageRankEigenVec.length; i++) {
            writer.write(String.format("%s%s%s%.3f%s\n",
                    "O PageRank de ",
                    nos[i].split(Configs.SEPARADOR_DADOS)[1],
                    " é ",
                    PageRankEigenVec[i],
                    ".")
            );
        }
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
    }

    private static void guardarPageRank(double[][] PageRank, BufferedWriter writer, double damp) throws IOException {
        writer.write("======================= PageRank (Iterativo) =====================");
        writer.newLine();
        writer.write(String.format("====================== Damping factor = %.2f =====================", damp));
        writer.newLine();
        //imprimir nome dos nós
        writer.write(String.format("%1$4s %2$" + String.valueOf(PageRank.length - 1).length() + "s ", "", ""));

        for (int i = 0; i < FileManager.nos.length; i++) {
            writer.write(String.format("%1$6s ", FileManager.nos[i].split(Configs.SEPARADOR_DADOS)[0]));
        }

        writer.newLine();

        for (int i = 0; i < PageRank.length; i++) {
            writer.write(String.format("%s %2$" + String.valueOf(PageRank.length - 1).length() + "s ", "Ite.", i));
            for (int j = 0; j < PageRank[0].length; j++) {
                writer.write(String.format("% 2.3f ", PageRank[i][j]));
            }
            writer.newLine();
        }
        writer.newLine();
        writer.write("==================================================================");
        writer.newLine();
    }

    public static void validaFicheiros(String nomeFNos, String nomeFRamos) {
        try {
            Scanner fNos = new Scanner(new File(nomeFNos));
            Scanner fRamos = new Scanner(new File(nomeFRamos));
            String primeiraLNos = fNos.nextLine();
            String primeiraLRamos = fRamos.nextLine();
            setNetworkType(primeiraLRamos);
            String segundaLRamos = fRamos.nextLine();
            int lNos = 0;
            int lRamos = 0;
            if (primeiraLNos.split(Configs.SEPARADOR_DADOS).length == Configs.N_CAMPOS_FICH_NOS
                    && segundaLRamos.split(Configs.SEPARADOR_DADOS).length == Configs.N_CAMPOS_FICH_RAMOS) {
                if (fNos.nextLine().trim().length() == 0) {
                    while (fNos.hasNextLine()) {
                        String linha = fNos.nextLine();
                        // Verifica se linha não está em branco e tem os 5 campos
                        if ((linha.trim()).length() > 0 && linha.split(Configs.SEPARADOR_DADOS).length == Configs.N_CAMPOS_FICH_NOS) {
                            lNos++;
                        } else {
                            System.out.println("Ficheiro Nos inválido. Linhas em branco no decorrer ou no final dos dados. A terminar...");
                            System.exit(0);
                        }
                    }
                }
                if (fRamos.nextLine().trim().length() == 0) {
                    while (fRamos.hasNextLine()) {
                        String linha = fRamos.nextLine();
                        // Verifica se linha não está em branco e tem os 3 campos
                        if ((linha.trim()).length() > 0 && linha.split(Configs.SEPARADOR_DADOS).length == Configs.N_CAMPOS_FICH_RAMOS) {
                            lRamos++;
                        } else {
                            System.out.println("Ficheiro Ramos inválido. Linhas em branco no decorrer ou no final dos dados. A terminar...");
                            System.exit(0);
                        }
                    }
                }
                if (Utils.contarLinhasFicheiro(nomeFNos) != lNos || Utils.contarLinhasFicheiro(nomeFRamos) != lRamos) {
                    System.out.println("Os dados contidos nos ficheiros encontram-se com erros...");
                    System.exit(0);
                }
            }
        } catch (Exception e) {
            System.out.println("ERRO na leitura do ficheiro: " + e.getMessage() + ".");
        }

    }

    public static void setNetworkType(String primeiraLRamos) {
        String linha[] = primeiraLRamos.split(":");
        if (linha[0].trim().equals("networkType") && (linha[1].trim().equals("oriented") || linha[1].trim().equals("nonoriented"))) {
            netWorkType = linha[1].trim();
        } else {
            System.out.println("A networkType não se encontra definida - a terminar.");
            System.exit(0);
        }
    }
}
