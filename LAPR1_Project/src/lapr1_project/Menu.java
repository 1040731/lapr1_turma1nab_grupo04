/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1_project;

import java.io.IOException;
import java.util.Scanner;

/**
 * @authors 
 * 
 * LAPR1 2018/2019 
 * Grupo 04 
 * 
 * 1970570 Claudia Monteiro 
 * 1040321 Pedro Barbosa 
 * 1040731 Pedro Sequeira 
 * 1080510 Miguel Silva
 *
 * Classe utilizada para gerar todos os menus da aplicação.
 */
public class Menu {

    private static String[] options = new String[8];
    private static String[] optionsOriented = new String[6];
    private static Scanner in = new Scanner(System.in);

    public static void executarMenuPrincipal(int[][] mat) throws IOException {

        options[0] = "\n1. Listar conteúdo dos ficheiros";
        options[1] = "2. Calcular todas as medidas e enviar para o ficheiro";
        options[2] = "3. Grau de um nó (Node Degree)";
        options[3] = "4. Centralidade de Vetor Próprio (Eigenvector Centrality)";
        options[4] = "5. Grau Médio (Average Degree)";
        options[5] = "6. Densidade (Density)";
        options[6] = "7. Potências da Matriz de Adjacências \"k\" (Powers of the adjacency matrix)";
        options[7] = "0. Sair\n";

        int op;

        do {

            espacamentoMenu();

            for (int i = 0; i < options.length; i++) {
                System.out.println(options[i]);
            }

            System.out.println("O que pretende calcular?\n");

            while (!in.hasNextInt()) {
                System.out.println("Valor inválido. Só são aceites números. Introduza novamente.");
                in.next();
            }

            op = in.nextInt();
            in.nextLine();

            switch (op) {

                case 1:
                    espacamentoMenu();
                    System.out.println("\n============= Ficheiro de NOS ===============");
                    Utils.listarConteudoFicheiro(FileManager.nos);
                    System.out.println("\n============ Ficheiro de RAMOS ==============");
                    System.out.println("======== Tipo de Rede = " + FileManager.netWorkType + " =========");
                    Utils.listarConteudoFicheiro(FileManager.ramos);
                    subMenu(mat);
                    break;

                case 2:
                    espacamentoMenu();
                    System.out.println("Qual o número de caminhos que pretende calcular?");
                    int path = in.nextInt();
                    in.nextLine();
                    SNA.calculateAll(mat, FileManager.sizeMatriz, path);
                    espacamentoMenu();
                    System.out.println("Todos os cálculos foram efetuados. Verifique ficheiro de saida.");
                    subMenu(mat);
                    break;

                case 3:
                    espacamentoMenu();
                    int max = 0;
                    int indexMax = 0;
                    for (int i = 0; i < FileManager.nos.length; i++) {
                        int NodeDegree = SNA.calculateNodeDegree(i, mat);
                        if (NodeDegree > max) {
                            max = NodeDegree;
                            indexMax = i;
                        }
                        System.out.printf("%s%s%s%d%s\n", "O grau de ",
                                FileManager.nos[i].split(Configs.SEPARADOR_DADOS)[1],
                                " é ",
                                NodeDegree,
                                ".");
                    }
                    Utils.openPage(Utils.getUrlForNode(indexMax, FileManager.nos));
                    subMenu(mat);
                    break;

                case 4:
                    espacamentoMenu();
                    double eigenVec[] = SNA.calculateEigenVecCent(mat);
                    if (eigenVec.length == 0) {
                        System.out.println("Não foi possivel encontrar um valor próprio maior que 0.");
                    } else {
                        for (int i = 0; i < eigenVec.length; i++) {
                            System.out.printf("%s%s%s%.3f%s\n", "A centralidade de ",
                                    FileManager.nos[i].split(Configs.SEPARADOR_DADOS)[1],
                                    " é ",
                                    eigenVec[i],
                                    ".");
                        }
                        Utils.openPage(Utils.getUrlForNode(Utils.calcularIndexMaiorValorVector(eigenVec), FileManager.nos));
                    }
                    subMenu(mat);
                    break;

                case 5:
                    espacamentoMenu();
                    System.out.printf("A média dos graus de todos os nós da rede é %.3f.\n", SNA.calculateAverageDegree(mat));
                    subMenu(mat);
                    break;

                case 6:
                    espacamentoMenu();
                    System.out.printf("O valor do nível geral de conectividade da rede (densidade) é %f.\n", SNA.calculateDensity(mat));
                    subMenu(mat);
                    break;

                case 7:
                    espacamentoMenu();
                    System.out.println("Qual o tamanho do caminho que pretende calcular? \n");

                    while (!in.hasNextInt()) {
                        System.out.println("Valor inválido.Só são aceites números. Introduza novamente.");
                        in.next();
                    }
                    int tamanho = in.nextInt();
                    while (tamanho <= 0) {
                        System.out.println("O valor não pode ser 0 - introduza novamente");
                        while (!in.hasNextInt()) {
                            System.out.println("O valor deve ser um Inteiro - introduza novamente");
                            in.next();
                        }
                        tamanho = in.nextInt();
                    }
                    espacamentoMenu();
                    System.out.println("== Matrizes de Potências == \n");
                    for (int a = 1; a <= tamanho; a++) {
                        System.out.printf("Matriz de potência %d \n\n", a);
                        Utils.listarMatriz((SNA.calculatePowersAM(a, mat)), mat.length);
                        System.out.println();
                    }
                    subMenu(mat);
                    break;

                case 0:
                    System.exit(0);
                    break;
            }

        } while (op != 0);
    }

    public static void executarMenuPrincipalOrientado(int[][] mat) throws IOException {

        optionsOriented[0] = "\n1. Listar conteúdo dos ficheiros";
        optionsOriented[1] = "2. Calcular todas as medidas e enviar para o ficheiro";
        optionsOriented[2] = "3. Grau de entrada de um nó (In-degree)";
        optionsOriented[3] = "4. Grau de saída de um nó (Out-degree)";
        optionsOriented[4] = "5. Page Rank";
        optionsOriented[5] = "0. Sair\n";

        int op;

        do {

            espacamentoMenu();

            for (int i = 0; i < optionsOriented.length; i++) {
                System.out.println(optionsOriented[i]);
            }

            System.out.println("O que pretende calcular?\n");

            while (!in.hasNextInt()) {
                System.out.println("Valor inválido. Só são aceites números. Introduza novamente.");
                in.next();
            }

            op = in.nextInt();
            in.nextLine();

            switch (op) {

                case 1:
                    espacamentoMenu();
                    System.out.println("\n============= Ficheiro de NOS ===============");
                    Utils.listarConteudoFicheiro(FileManager.nos);
                    System.out.println("\n============ Ficheiro de RAMOS ==============");
                    System.out.println("========= Tipo de Rede = " + FileManager.netWorkType + " ===========");
                    Utils.listarConteudoFicheiro(FileManager.ramos);
                    subMenuOriented(mat);
                    break;

                case 2:
                    espacamentoMenu();
                    System.out.println("Qual o número de iterações que pretende calcular?");
                    while(!in.hasNextInt()){
                        System.out.println("O valor deve ser um Inteiro - introduza novamente");
                        in.next(); 
                    }
                    int iterations = in.nextInt();
                    while (iterations <= 0) {
                        System.out.println("O valor não pode ser 0 - introduza novamente");
                        while (!in.hasNextInt()) {
                            System.out.println("O valor deve ser um Inteiro - introduza novamente");
                            in.next();
                        }
                        iterations = in.nextInt();
                    }
                    in.nextLine();
                    espacamentoMenu();
                    System.out.println("Qual o valor do damping factor?");
                    while(!in.hasNextDouble()){
                        System.out.println("O valor deve ser um decimal - introduza novamente");
                        in.next(); 
                    }
                    double damp = in.nextDouble();
                    while(damp  <= 0 || damp >1){
                        System.out.println("O valor tem de ser entre 0 e 1 - introduza novamente");
                        while(!in.hasNextDouble()){
                        System.out.println("O valor deve ser um decimal - introduza novamente");
                        in.next(); 
                    }
                        damp = in.nextDouble();
                    }
                    in.nextLine();
                    SNA.calculateAllOriented(mat, FileManager.sizeMatriz, iterations +1 , damp);
                    espacamentoMenu();
                    System.out.println("Todos os cálculos foram efetuados. Verifique ficheiro de saida.");
                    subMenuOriented(mat);
                    break;

                case 3:
                    espacamentoMenu();
                    int max = 0;
                    int indexMax = 0;
                    for (int i = 0; i < FileManager.nos.length; i++) {
                        int NodeinDegree = SNA.calculateNodeDegree(i, mat);
                        if (NodeinDegree > max) {
                            max = NodeinDegree;
                            indexMax = i;
                        }
                        System.out.printf("%s%s%s%d%s\n", "O grau de entrada de ",
                                FileManager.nos[i].split(Configs.SEPARADOR_DADOS)[1],
                                " é ",
                                NodeinDegree,
                                ".");
                    }
                    Utils.openPage(Utils.getUrlForNode(indexMax, FileManager.nos));
                    subMenuOriented(mat);
                    break;

                case 4:
                    espacamentoMenu();
                    max = 0;
                    indexMax = 0;
                    for (int i = 0; i < FileManager.nos.length; i++) {
                        int NodeOutDegree = SNA.calculateNodeOutDegree(i, mat);
                        if (NodeOutDegree > max) {
                            max = NodeOutDegree;
                            indexMax = i;
                        }
                        System.out.printf("%s%s%s%d%s\n", "O grau de saída de ",
                                FileManager.nos[i].split(Configs.SEPARADOR_DADOS)[1],
                                " é ",
                                NodeOutDegree,
                                ".");
                    }
                    Utils.openPage(Utils.getUrlForNode(indexMax, FileManager.nos));
                    subMenuOriented(mat);
                    break;

                case 5:
                    espacamentoMenu();
                    subMenuPageRank(mat);
                    subMenuOriented(mat);
                    break;
                case 66:
                    System.out.println("Qual o tamanho do caminho que pretende calcular? \n");

                    while (!in.hasNextInt()) {
                         System.out.println("Valor inválido. Só são aceites números. Introduza novamente.");
                        in.next();
                    }
                    int tamanho = in.nextInt();
                    espacamentoMenu();
                    System.out.println("== Matrizes de Potências == \n");
                    for (int a = 1; a <= tamanho; a++) {
                        System.out.printf("Matriz de potência %d \n\n", a);
                        Utils.listarMatriz((SNA.calculatePowersAM(a, mat)),mat.length);
                        System.out.println();
                    }
                    subMenuOriented(mat);
                    break;
                case 67:
                    System.out.println("Qual o número de iterações que pretende calcular? \n");

                    while (!in.hasNextInt()) {
                        System.out.println("Valor inválido. Só são aceites números. Introduza novamente.");
                        in.next();
                    }
                    int k = in.nextInt();
                    espacamentoMenu();
                    System.out.println("Qual o valor de damping factor que pretende? \n");

                    while (!in.hasNextDouble()) {
                        System.out.println("Valor inválido. Só são aceites números. Introduza novamente.");
                        in.next();
                    }
                    double dampingFactor = in.nextDouble();
                    SNA.calculateEuclidianeDistance(mat, dampingFactor, k);
                    subMenuOriented(mat);
                    break;

                case 0:
                    System.exit(0);
                    break;
            }

        } while (op != 0);
    }

    private static void espacamentoMenu() {

        System.out.printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

    private static void subMenu(int[][] mat) throws IOException {

        System.out.printf("\n"
                + "1. Voltar ao menu\n"
                + "2. Sair\n");

        int op;

        do {

            while (!in.hasNextInt()) {
                System.out.println("Valor inválido. Só são aceites números. Introduza novamente.");
                in.next();
            }

            op = in.nextInt();
            in.nextLine();
            switch (op) {
                case 1:
                    executarMenuPrincipal(mat);
                    op = 0;
                    break;
                case 2:
                    System.exit(0);
                    op = 0;
                default:
                    System.out.println("Opção errada. Escolha novamente.");
                    break;
            }
        } while (op != 0);
    }

    private static void subMenuOriented(int[][] mat) throws IOException {

        System.out.printf("\n"
                + "1. Voltar ao menu\n"
                + "2. Sair\n");

        int op;

        do {

            while (!in.hasNextInt()) {
                System.out.println("Valor inválido. Só são aceites números. Introduza novamente.");
                in.next();
            }

            op = in.nextInt();
            in.nextLine();
            switch (op) {
                case 1:
                    executarMenuPrincipalOrientado(mat);
                    op = 0;
                    break;
                case 2:
                    System.exit(0);
                    op = 0;
                default:
                    System.out.println("Opção errada. Escolha novamente.");
                    break;
            }
        } while (op != 0);
    }

public static boolean menuConfirmacao() {
        int op;
        boolean res = false;
        String texto = "Dados repetidos. Deseja continuar?"
                + "\n 1 - Sim"
                + "\n 2 - Não";
        System.out.printf("%n%s%n", texto);

        do {
            op = in.nextInt();
            in.nextLine();
            switch (op) {
                case 1:
                    res = true;
                    op = 0;
                    break;
                case 2:
                    res = false;
                    op = 0;
                    break;
                default:
                    System.out.println("Opção Inválida.");
                    break;
            }
        } while (op != 0);
        return res;
    }

    private static void subMenuPageRank(int[][] mat) throws IOException {
        System.out.println("Qual o método que pretende utilizar?\n");

        System.out.printf(""
                + "1. Vector próprio\n"
                + "2. Iterativo\n"
                + "3. Sair\n");
        int op;
        do {
            while (!in.hasNextInt()) {
                System.out.println("Valor inválido. Só são aceites números. Introduza novamente.");
                in.next();
            }
            op = in.nextInt();
            in.nextLine();
            switch (op) {
                case 1:
                    espacamentoMenu();
                    System.out.println("Qual o valor de damping factor que pretende? \n");

                    while (!in.hasNextDouble()) {
                        System.out.println("Valor inválido. Só são aceites números. Introduza novamente.");
                        in.next();
                    }
                    double dampingFactor = in.nextDouble();
                    while (dampingFactor <= 0 || dampingFactor > 1) {
                        System.out.println("O valor tem de ser entre 0 e 1 - introduza novamente");
                        while (!in.hasNextDouble()) {
                            System.out.println("O valor deve ser um decimal - introduza novamente");
                            in.next();
                        }
                        dampingFactor = in.nextDouble();
                    }
                    in.nextLine();
                    double pageRankVec[] = SNA.calculatePageRankEigenVec(mat, dampingFactor);
                    espacamentoMenu();
                    System.out.printf("== Damping factor = %.2f == \n\n", dampingFactor);

                    //imprimir o vector proprio
                    Utils.printPageRankEigenVec(pageRankVec);
                    
                    if(pageRankVec.length > 0){
                         Utils.openPage(Utils.getUrlForNode(Utils.calcularIndexMaiorValorVector(pageRankVec), FileManager.nos));
                    }
                  
                    subMenuOriented(mat);
                    break;
                case 2:
                    espacamentoMenu();
                    System.out.println("Qual o número de iterações que pretende calcular? \n");

                    while (!in.hasNextInt()) {
                        System.out.println("Valor inválido. Só são aceites números. Introduza novamente.");
                        in.next();
                    }
                    int k = in.nextInt();
                    while (k <= 0) {
                        System.out.println("O valor não pode ser 0 - introduza novamente");
                        while (!in.hasNextInt()) {
                            System.out.println("O valor deve ser um Inteiro - introduza novamente");
                            in.next();
                        }
                        k = in.nextInt();
                    }
                    in.nextLine();
                    espacamentoMenu();
                    System.out.println("Qual o valor de damping factor que pretende? \n");

                    while (!in.hasNextDouble()) {
                        System.out.println("Valor inválido. Só são aceites números. Introduza novamente.");
                        in.next();
                    }
                    dampingFactor = in.nextDouble();
                    while (dampingFactor <= 0 || dampingFactor > 1) {
                        System.out.println("O valor tem de ser entre 0 e 1 - introduza novamente");
                        while (!in.hasNextDouble()) {
                            System.out.println("O valor deve ser um decimal - introduza novamente");
                            in.next();
                        }
                        dampingFactor = in.nextDouble();
                    }
                    in.nextLine();
                    espacamentoMenu();
                    System.out.println("======== Page Rank ========");
                    System.out.printf("== Damping factor = %.2f == \n", dampingFactor);
                    System.out.println();

                    double[][] pageRankMat = SNA.calculatePageRankIteration(mat, k + 1 , dampingFactor);
                    
                    // imprimir as iteraçoes
                    Utils.printPageRankIteration(pageRankMat);
                    
                    Utils.openPage(Utils.getUrlForNode(Utils.calcularIndexMaiorValorVector(pageRankMat[k-1]), FileManager.nos));

                    subMenuOriented(mat);
                    break;
                case 3:
                    System.exit(0);
                    op = 0;
                default:
                    System.out.println("Opção errada. Escolha novamente");
                    break;
            }
        } while (op != 0);
    }
}
